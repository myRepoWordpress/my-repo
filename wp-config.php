<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'WPCACHEHOME', 'C:\OSPanel\domains\myProject.Repo\wp-content\plugins\wp-super-cache/' );
define('DB_NAME', 'MyRepo');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Vh@qa!65NJ20{sBaP`4bfpry=1xR[#PgdqflyVn{sbv$,ha9HG[_K,Hr_IZN=dH1');
define('SECURE_AUTH_KEY',  ':$PM#p~,4EyCuUz~`lNn^H|gv$b$#O>,U[^{KLGR$KfY&@RvS^c&(}QB`z&Hni`D');
define('LOGGED_IN_KEY',    '|s{0w/)NwgG6]J/nZk&.`dC8hx:1!VGo00Q@wb5+dQ8 R$tYcj)}W8VcE<+f!v#*');
define('NONCE_KEY',        '*63:%Mz9_LxF.0D7<Llg_x5L&GG9,24lbt OR,LCH8f@khmeI:9@6?JFj*opzJ$:');
define('AUTH_SALT',        'M_%FpJ=]3wfTBik#p]%I~FPA:l}bSQk4/vKH0%d.F!xm_4~}m+&NENef]$<$ncGC');
define('SECURE_AUTH_SALT', 'UtHVrGMbF|{yw0884HOxhsV1Q1#4B#y.~:7(e8Y2z^G#gyDx-W}lM^U4%u`f<]Y)');
define('LOGGED_IN_SALT',   'SI@X%qJo=Y-)8P@98pi]{kzh7FV5bD(}N/R(i?_F_>a,:WILJY[,IAd6Jv.H*ZX=');
define('NONCE_SALT',       '&IU[>WP2#9@%[ExBc{TqjXUXD|W#GM5Yhy$[`~ePKPrkyGlaePtrWiY(:^)Rbmg?');

/**#@-*/
// define('WP_HOME', 'http://137.74.240.28/');
// define('WP_SITEURL', 'http://137.74.240.28/');
// define( 'FORCE_SSL_ADMIN', false );

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_CACHE', true);
define('ENABLE_CACHE', true );
define('CACHE_EXPIRATION_TIME', 900);

/* Это всё, дальше не редактируем. Успехов! */
//редакци
define( 'WP_POST_REVISIONS', 0 );



/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
