// Обязательная обёртка
module.exports = function(grunt) {

  var template_url = '../wp-content/themes/lirax-theme/';

  // Задачи
  grunt.initConfig({
    // Склеиваем
    source: {
      less: {
        src: template_url + 'style/main.less',
        dest: template_url + 'style.css'
      }
    },
    concat: {
      main: {
        src: [
          template_url + 'assets/cookie/jquery.cookie.js',
          template_url + 'js/calc.js',
          template_url + 'js/main.js'
        ],
        dest: template_url + 'js/main.min.js'
      }
    },
    // Сжимаем
    uglify: {
      main: {
        files: {
          // Результат задачи concat
          '<%= concat.main.dest %>' : '<%= concat.main.dest %>'
        }
      }
    },
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          '<%= source.less.dest %>': '<%= source.less.src %>' // destination file and source file
        }
      }
    },
    watch: {
      concat: {
        files: [template_url + 'style/*.less', template_url + 'js/main.js', template_url + 'js/calc.js'],
        tasks: ['concat', 'less', 'uglify']
      }
    }
  });

  // Загрузка плагинов, установленных с помощью npm install
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Задача по умолчанию
  grunt.registerTask('default', ['concat', 'less', 'uglify', 'watch']);
};