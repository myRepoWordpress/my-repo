$(document).ready(function() {
	$('[data-action]').click(function(e){
		e.preventDefault();
		var target = $(this),
			action = target.data('action'),
			file = target.data('file');
		
		actionFunction(action, file, target);
	})
});


var actionFunction =  function(action, file, target){
    $.ajax({
		type: 'POST',
    	url: 'action.php',
    	data: {
    		action: action,
    		file: file
    	},
    	success: function(data){
            alert(data);
            location.reload();
    	},
    	error: function(){
    		console.log('Ajax error!');
    	}
    })
}