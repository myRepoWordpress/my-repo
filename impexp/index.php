<?php 

function ShowFiles($type) {
	if ($handle = opendir('../tmp/')) {
	    echo "<table>";
	    /* Именно этот способ чтения элементов каталога является правильным. */
	    while (false !== ($file = readdir($handle))) { 
	        $rest = substr($file, -3);
			$fileDump = explode("_", $file);
	        if ($rest == $type)
	        	echo 	"<tr>
		        			<td>$file</td>
		        			<td>
		        				<a href='javascript:;' data-action='restore_".$type."' data-file='".$file."'>Restore</a>
		        			</td>
		        			<td>
		        				<a href='javascript:;' data-action='delete_file' data-file='".$file."'>Delete</a>
		        			</td>
		        			<td>
		        				<a href='https://lirax.ua/tmp/".$file."' target='_blank'>Скачать</a>
		        			</td>
		        		</tr>";
	    }
	    echo "</table>";
	    closedir($handle); 
	}
}

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Импорт и экспорт данных</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.0.0/normalize.min.css"></link>
	<style>
		td{
			vertical-align: top;
		}
	</style>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>MySQL</th>
				<th>Files</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<a href="javascript:;" data-action="export_sql">Экспорт</a>
					<?php 
						ShowFiles('sql');
					?>
				</td>
				<td>
					<a href="javascript:;" data-action="export_zip">Экспорт</a>
					<div id="resault2"></div>
					<?php 
						ShowFiles('zip');
					?>
				</td>
			</tr>
		</tbody>
	</table>
	<div id="resault"></div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="main.js"></script>
</body>
</html>