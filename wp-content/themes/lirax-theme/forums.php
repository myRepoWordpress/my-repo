<?php
/*
Template Name: forums
*/

get_header(); ?>

	

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<div class="row">
					<div class="span-lg-12">
					
						<?php dynamic_sidebar('forum'); ?>

						<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'forum' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						endwhile; // End of the loop.
						?>
					</div>
					<?php //get_sidebar(); ?>
				</div>
				
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
