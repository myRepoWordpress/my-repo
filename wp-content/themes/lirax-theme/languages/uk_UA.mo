��          �      L      �     �  8   �      _        x     �     �     �  %   �     �     �     �  F   	     P     X     h     }     �  �  �  "   w  I   �  �  �  �   �     I	     f	  "   �	  !   �	  -   �	     �	     
     #
  i   8
     �
      �
     �
     �
     �
                 
      	                                                                             Comments are closed. Continue reading %s <span class="meta-nav">&rarr;</span> Hi. I'm a starter theme called <code>_s</code>, or <em>underscores</em>, if you like. I'm a theme meant for hacking so don't use me as a <em>Parent Theme</em>. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for. It looks like nothing was found at this location. Maybe try one of the links below or a search? Most Used Categories Newer Comments Nothing Found Older Comments Oops! That page can&rsquo;t be found. Pages: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Sidebar Skip to content Theme: %1$s by %2$s. _s https://wordpress.org/ Project-Id-Version: _s 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/tags/_s
POT-Creation-Date: 2016-08-06 10:21+0300
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-08-06 10:29+0300
Language-Team: 
X-Generator: Poedit 1.8.8
Last-Translator: zSlayer <marty.pinheads@gmail.com>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: uk_UA
 Коментарі закриті. Продовжити читати %s <span class="meta-nav">&rarr;</span> Привіт. Я стартер тема під назвою <code>_s</code> або <em>underscores</em> якщо хочете. Я тема призначена для злому , так що не використовувати мене в якості <em>Parent Theme</em>. Замість того, щоб спробувати повернути мене в наступний, найдивовижніший, WordPress тема там. Це те, що я тут. Нічого не знайдено на даній локації. Спробуйте одну з посилань нижче, або пошук? Найпопулярніші Новіші коментарі Нічого не Знайдено Старіші коментарі Ой! Сторінка не знайдена. Сторінки: Головне меню Працює на %s Готові публікувати ваш перший пост? <a href="%1$s">Почати тут</a>. Бічна колонка Перейти до вмісту Шаблон: %1$s by %2$s. _s https://uk.wordpress.org/ 