��    %      D  5   l      @  
   A     L  	   O  
   Y     d     w  8   �     �    �  _   �  W   6     �     �     �     �     �  %   �               !     )     6  F   L     �     �     �  \   �          +  )   @     j     m     �     �     �     �  �  �     �	     �	     �	  
   �	  0   �	  $   .
  ?   S
     �
  �  �
  �   �  �   2  -   �  6   �  )   +      U  +   v  +   �     �      �            !   ,  y   N  %   �     �  (     �   5     �     �  X   �     N     Q     h          �     �                                              
                                                	   !      "      $               #                                                %       % Comments ,  1 Comment Automattic Comment navigation Comments are closed. Continue reading %s <span class="meta-nav">&rarr;</span> Edit %s Hi. I'm a starter theme called <code>_s</code>, or <em>underscores</em>, if you like. I'm a theme meant for hacking so don't use me as a <em>Parent Theme</em>. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for. It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment on %s Most Used Categories Newer Comments Nothing Found Older Comments Oops! That page can&rsquo;t be found. Pages: Posted in %1$s Primary Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s Theme: %1$s by %2$s. Try looking in the monthly archives. %1$s _s http://automattic.com/ http://underscores.me/ https://wordpress.org/ post authorby %s post datePosted on %s Project-Id-Version: _s 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/tags/_s
POT-Creation-Date: 2016-08-05 22:26+0300
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-08-15 18:14+0300
Language-Team: 
X-Generator: Poedit 1.8.7
Last-Translator: zSlayer <marty.pinheads@gmail.com>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru_RU
 Комментарии: %s ,  1 комментарий Automattic Навигация по комментариям Обсуждение закрыто. Читать далее %s <span class="meta-nav">&rarr;</span> Редактировать %s Здравствуй. Я стартовый шаблон <code>_s</ code>, или <em>underscores</ em>, если хотите. Я предназначен для взлома, так что не использовать меня в качестве <em> Родитель Тема </ em>. Вместо того, чтобы попытаться повернуть меня в следующий, самый удивительный, WordPress тема там. Это то, что я здесь. Похоже, ничего не найдено. Может быть, попробовать одну из ссылок ниже или поиск? Запрошенную информацию найти не удалось. Возможно, будет полезен поиск по сайту. Оставить комментарий в %s Часто используемые категории Следующие комментарии Ничего не найдено Предыдущие комментарии Ой! Страница не найдена. Страницы: Опубликовано в %1$s Основной Основное меню Сайт работает на %s Готовы опубликовать свою первую запись? <a href="%1$s">Начните отсюда</a>. Результаты поиска: %s Боковая колонка Перейти к содержимому Извините, по вашему запросу ничего не найдено. Попробуйте другие ключевые слова. Метки %1$s Шблон: %1$s от %2$s. Попробуйте посмотреть в ежемесячных архивах. %1$s _s http://automattic.com/ http://underscores.me/ https://wordpress.org/ Автор: %s Дата: %s 