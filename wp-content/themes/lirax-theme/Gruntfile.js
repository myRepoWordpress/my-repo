// Обязательная обёртка
module.exports = function(grunt) {

  var template_url = '../wp-content/themes/lirax-theme/';

  // Задачи
  grunt.initConfig({
    // Склеиваем
    concat: {
      main: {
        src: [
          template_url + 'assets/cookie/jquery.cookie.js',
          template_url + 'js/calc.js',
          template_url + 'js/main.js'
          //'js/mylibs/**/*.js'  // Все JS-файлы в папке
        ],
        dest: template_url + 'js/main.min.js'
      }
    },
    // Сжимаем
    uglify: {
      main: {
        files: {
          // Результат задачи concat
          template_url + 'js/main.min.js': '<%= concat.main.dest %>'
        }
      }
    },
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "style.css": template_url + "style/main.less" // destination file and source file
        }
      }
    },
    watch: {
      concat: {
        files: ['style/*.less', 'js/main.js', 'js/calc.js'],
        tasks: ['concat', 'less', 'uglify',]  // Можно несколько: ['lint', 'concat']
      }
    }
  });

  // Загрузка плагинов, установленных с помощью npm install
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Задача по умолчанию
  grunt.registerTask('default', ['concat', 'less', 'uglify', 'watch']);
};