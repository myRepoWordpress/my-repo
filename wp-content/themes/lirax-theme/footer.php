<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lirax-theme
 */

?>
	</div><!-- #content -->
	


	<!-- Футер -->
	<div class="wrap-block footer-block">
		<?php if (is_active_sidebar('footer')) : ?>
		<div class="container">
			<div class="row hidden-xx">
				<div class="span-xx-12">
					<div class="tablestyle">
						<?php dynamic_sidebar('footer'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php 
			$locale = get_locale();
				if ( $locale =='en_US' ) { 
				printf( '<div class="copyright-block">liraX 2016 © Все права защищены.</div>');} 
				elseif ( $locale =='uk_UA' ) {
				printf( '<div class="copyright-block">liraX 2016 © Всі права захищені.</div>');}
				else {
				printf( '<div class="copyright-block">liraX 2016 © Все права защищены.</div>');}?>
		
	</div>
	<!--/ Футер -->

	<div class="modal lirax-modal">
		<div id="liraxModal" class="form-modal">
			<div class="row modal-body">
				<div class="span-xs-2 span-md-3 hidden-xx">
					<div class="modal-icon">
						<img src="<?=get_template_directory_uri()?>/images/grey/connect-circle.png" id="modalIcon" class="retina-ready">
					</div>
				</div>
				<div class="span-xs-10 span-md-9">
					<div class="modal-title" id="modalTitle"></div>
					<div class="modal-subtitle" id="modalSubTitle"></div>
					
					<div class="dataBlock"></div>
					<form class="form ajax-form lirax-custom-form" id="modalForm">
						<div class="modal-field field-group" id="modalField">
<?php 			$locale = get_locale();
				if ( $locale =='uk_UA' ) { 
				printf('<input type="text" name="name" data-name="Имя" placeholder="Ім&#39;я" id="liraxModalInputName" required>');} 
				else {
				printf('<input type="text" name="name" data-name="Имя" placeholder="Имя" id="liraxModalInputName" required>');}?>

							
							<input type="text" name="phone" data-name="Телефон" class="phone-mask" id="liraxModalInputPhone" placeholder="380" required>
							
							<input type="hidden" name="subject" data-name="Тема" id="modalSubject" value="">
							<input type="hidden" name="main_message" id="main_message" data-name="Сообщение" value="">

<?php 			$locale = get_locale();
				if ( $locale =='uk_UA' ) { 
				printf('<button type="submit" form="modalForm" value="Submit" class="btn btn-lightblue btn-round hidden-xs">Відправити <i class="icons paper_fly icons-right"></i></button>');} 
				else {
				printf('<button type="submit" form="modalForm" value="Submit" class="btn btn-lightblue btn-round hidden-xs">Отправить <i class="icons paper_fly icons-right"></i></button>');}?>
							
							<button type="submit" form="modalForm" value="Submit" class="btn btn-lightblue btn-round hidden-xx visible-xs"><i class="icons paper_fly icons-right"></i></button>
						</div>
					</form>
				</div>
			</div>
			
			<div class="modal-footer">
				<?php 			$locale = get_locale();
				if ( $locale =='uk_UA' ) { 
				printf('Бізнес IP-телефонія');} 
				else {
				printf('Бизнес IP-телефония');}?> <a href="https://lirax.com.ua/callback" target="blank"><img src="<?=get_template_directory_uri()?>/images/elements/logo-modal.png" class="retina-ready"></a>
			</div>
			<a href="#" class="close-fancybox modal-close"><i class="fa fa-times"></i></a>
		</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>
<?php //get_template_part('remarketing'); ?>
<?php
$languages = array('uk');
if ($_COOKIE['lang']) {
	$l = $_COOKIE['lang'];
} else {
	$lang = explode(";", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
	if ($lang[0]) {
		$l = explode(",", $lang[0])[0];
	}
}
setcookie("lang", $l, '/', $_SERVER['HTTP_HOST']);
if (in_array($l, $languages) && ($l != $_COOKIE['_icl_current_language']) && !isset($_COOKIE['redirect'])) {
    setcookie("redirect", 1, time()+86400, '/', $_SERVER['HTTP_HOST']);
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/'.$l.$_SERVER['REQUEST_URI']);
    exit();
// print_r($_COOKIE);
}


?>
</body>
</html>
