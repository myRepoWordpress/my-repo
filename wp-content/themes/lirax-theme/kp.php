<?php

/*
Template Name: kp-page
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Коммерческое предложение</title>
    <script>
        if (window.location.protocol == "https:")
        window.location.href = "http:" + window.location.href.substring(window.location.protocol.length);
    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&subset=cyrillic,latin-ext" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="<?=get_template_directory_uri()?>/style/kp.less" />
    <script src="<?=get_template_directory_uri()?>/assets/less/less.min.js" type="text/javascript"></script>
</head>
<body>
    
    <!-- <div class="print-container"> -->
        <!-- <div class="print-page"> -->
            <div class="static">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus excepturi amet laboriosam, nam atque magnam, velit et molestias. Incidunt deleniti nulla at, mollitia ea ipsum, impedit quibusdam aliquam optio quam!
            </div>


            <?php 
                while ( have_posts() ) : the_post();
                    remove_filter( 'the_content', 'wpautop' );
                    the_content(); 
                endwhile; // End of the loop.
            ?>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis nihil autem unde quas eligendi libero tempore aperiam enim illo aut, maiores perferendis. Minus eveniet accusamus necessitatibus recusandae modi. Quam, eligendi.</p>
        <!-- </div> -->
    <!-- </div> -->

    
    
</body>
</html>

	

<?php 
