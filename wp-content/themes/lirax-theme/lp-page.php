<?php

/*
Template Name: lp-page
*/
get_header();
?>

	<?php while ( have_posts() ) : the_post(); ?>
	
	<?php 
		remove_filter( 'the_content', 'wpautop' );
		the_content(); 
	?>

	<?php endwhile; // End of the loop.?>

<?php 
get_footer();
