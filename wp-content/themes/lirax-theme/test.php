<?php

/*
Template Name: test-page
*/
get_header();
?>

<div id="localSolutions">
    <div class="wrap-block section-1">
        <div class="container">
            <h1 class="title">Локальные решения LiraX</h1>
            <div class="subtitle">Почувствуйте все преимущества сервисов LiraX в локальном решении</div>
            <div class="action-block"><a href="#"  data-action="click" onclick="LIRAX.showWidget(); return false;" class="btn btn-round border-lightblue text-lightblue border2x">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</a></div>

            <a href="#section-2" class="next-block-btn scrollToID"><img src="<?=this_tamplate()?>/images/local-solution/down.png" alt="down-btn"></a>
        </div>
    </div>

    <div class="wrap-block section-2" >
        <div class="container">
            <div class="title" id="section-2">У нас есть решения для любых задач</div>
            <div class="subtitle">Готовые локальные решения увеличивают эффективность бизнеса от 17%</div>
            <ul class="solution-list">
                <li>
                    <div class="s-block">
                        <div class="s-img"><img src="<?=this_tamplate()?>/images/local-solution/ipatc-icon.png" alt="IP ATC"></img></div>
                        <div class="s-name">IP ATC</div>
                    </div>
                </li>
                <li>
                    <div class="s-block">
                        <div class="s-img"><img src="<?=this_tamplate()?>/images/local-solution/crm-icon.png" alt="+IP ATC +Call Center"></img></div>
                        <div class="s-name">CRM LiraX</div>
                        <div class="s-subname">+IP ATC<br> +Call Center</div>
                    </div>
                </li>
                <li>
                    <div class="s-block">
                        <div class="s-img"><img src="<?=this_tamplate()?>/images/local-solution/callcenter-icon.png" alt="CallCenter"></img></div>
                        <div class="s-name">CallCenter</div>
                    </div>
                </li>
                <li>
                    <div class="s-block">
                        <div class="s-img"><img src="<?=this_tamplate()?>/images/local-solution/videoserver-icon.png" alt="Видеоконференц сервер"></img></div>
                        <div class="s-name">Видеоконференц сервер</div>
                    </div>
                </li>
                <li>
                    <div class="s-block">
                        <div class="s-img"><img src="<?=this_tamplate()?>/images/local-solution/gsm-icon.png" alt="GSM шлюз"></img></div>
                        <div class="s-name">GSM шлюз</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="wrap-block section-3">
        <div class="container">
            <p>Наша компания имеет богатый опыт в сфере установки ПО локальных коллцентров, построения отделов обработки обращений клиентов. Эти и другие решения работают на платформе Lira, которая прекрасно себя зарекомендовала при больших нагрузках, легко масштабируется и гибкая в настройках и интеграции с другими базами данных и системами</p>

            <p>Также нами разработано и активно внедряется ПО для Видеоконференций. Особеностю является работа в веб браузере, отсутствие привязки к производителю оборудования и возможность развертывания на ИТ инфраструктуре Заказчика.</p>
        </div>
    </div>

    <div class="wrap-block section-4">
        <div class="container">
            <div class="title">Для подробной консультации оставьте заявку</div>
            <div class="subtitle">или обратитесь напрямую к менеджерам</div>

            <form class="material-form text-white lirax-custom-form">
                <div class="row hiden-on-submit">
                    <div class="span-sm-4 mb-xs-30 mb-xx-30 span-lg-3 offset-lg-1">
                        <!-- Вот эта штука и заменит нам select -->
                        <div class="selectGeneral">
                            <label class="selectGeneral">
                                <input type="radio" name="solution">
                                <div>
                                    <input
                                        type="radio"
                                        name="solution"
                                        value="Локальное решение: IP АТС"
                                        id="vs-1"
                                        checked
                                    >
                                    <label for="vs-1">IP АТС</label>

                                    <input
                                        type="radio"
                                        name="solution"
                                        value="Локальное решение: CRM LiraX"
                                        id="vs-2"
                                    >
                                    <label for="vs-2">CRM LiraX</label>

                                    <input
                                        type="radio"
                                        name="solution"
                                        value="Локальное решение: CallCenter"
                                        id="vs-3"
                                    >
                                    <label for="vs-3">CallCenter</label>

                                    <input
                                        type="radio"
                                        name="solution"
                                        value="Локальное решение: Видеоконференц сервер"
                                        id="vs-4"
                                    >
                                    <label for="vs-4">Видеоконференц сервер</label>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="span-sm-4 mb-xs-30 mb-xx-30 span-lg-3">
                        <input type="text" placeholder="Ваше имя" name="name" autocomplete="off">
                    </div>
                    <div class="span-sm-4 mb-xs-30 mb-xs-30 span-lg-3">
                        <input type="text" placeholder="Номер телефона" name="phone" autocomplete="off">
                    </div>
                    <div class="action-btn span-xx-12">
                        <button type="submit" class="btn btn-round btn-border-white hidden-xs hidden-xx">ПОЛУЧИТЬ ПОДРОБНУЮ КОНСУЛЬТАЦИЮ</button>
                        <button type="submit" class="btn btn-round btn-border-white hidden-sm hidden-md hidden-lg">ПОЛУЧИТЬ  КОНСУЛЬТАЦИЮ</button>
                    </div>
                </div>
                
                
            </form>
        </div>
    </div>
</div>

<?php 
get_footer();
