<?php
/**
 * lirax-theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package lirax-theme
 */

// <!--[START] Чистим вордпрес от мусора 
	remove_action('wp_head', 'wp_generator'); 
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'rsd_link'); 
	remove_action('wp_head', 'feed_links_extra', 3 );
	remove_action('wp_head', 'feed_links', 2 );
	remove_action('wp_head', 'index_rel_link' ); 
	remove_action('wp_head', 'parent_post_rel_link', 10, 0 ); 
	remove_action('wp_head', 'start_post_rel_link', 10, 0 ); 
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0 );
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );


	function rem_wp_ver_css_js( $src ) {
	    if ( strpos( $src, 'ver=' ) )
	        $src = remove_query_arg( 'ver', $src );
	    return $src;
	}
	add_filter( 'style_loader_src', 'rem_wp_ver_css_js', 9999 );
	add_filter( 'script_loader_src', 'rem_wp_ver_css_js', 9999 );
// [END] --> Чистим вордпрес от мусора 

// <!--[START] загружаем css во фронтэнде сайта
function lirax_scripts() {
	$my_theme_version = wp_get_theme()->get( 'Version' );
	wp_enqueue_style('cdn-lirax-roboto', roboto_font_url(), array(), null );

	wp_enqueue_style('cdn-font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('cdn-normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/4.0.0/normalize.min.css');
	wp_enqueue_style('cdn-bxslider', 'https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.css');
	wp_enqueue_style('cdn-tooltipster-theme', 'https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/themes/tooltipster-light.min.css');
	wp_enqueue_style('cdn-jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css');
	
	wp_enqueue_style('fancybox', get_template_directory_uri().'/assets/fancybox/source/jquery.fancybox.css?v=2.1.5');
	wp_enqueue_style('flaticon-callback', get_template_directory_uri().'/style/flaticon-callback/flaticon.css');
	wp_enqueue_style('flaticon-calltracking', get_template_directory_uri().'/style/flaticon-calltracking/flaticon.css');

	wp_enqueue_style( 'style-main', get_stylesheet_uri().'?v='.$my_theme_version);

  wp_deregister_script('jquery' );
  wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js', array(), '2.2.2', true);
  wp_enqueue_script('jquery');

  wp_enqueue_script('cdn-sendpulse', 'https://cdn.sendpulse.com/js/push/42bd3b84918764003fcc33a561b770c4_1.js', array(), false, false);
  wp_enqueue_script('cdn-jqueryui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js', array(), false, true);
  wp_enqueue_script('cdn-selectivizr', 'https://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js', array(), false, true);
  wp_enqueue_script('cdn-prefixfree', 'https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js', array(), false, true);
  wp_enqueue_script('cdn-bxslider', 'https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.js', array(), false, true);
  wp_enqueue_script('cdn-jquerymask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js', array(), false, true);
  wp_enqueue_script('cdn-scrollto', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js', array(), false, true);

  // Снег
  // wp_enqueue_script('cdn-snow', 'https://cdnjs.cloudflare.com/ajax/libs/JQuery-Snowfall/1.7.4/snowfall.jquery.min.js', array(), false, true);

  
  // wp_enqueue_script('cdn-googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAT7K_Qw7eB3fWhVVHZZugPxXV8MLKDiFc&callback=initMap', array(), false, true);
  
  wp_enqueue_script('fancybox', get_template_directory_uri().'/assets/fancybox/source/jquery.fancybox.pack.js?v=2.1.5', array(), false, true);
  wp_enqueue_script('fancybox-media', get_template_directory_uri().'/assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6', array(), false, true);
  wp_enqueue_script('sourcebuster', get_template_directory_uri().'/assets/sourcebuster-js/dist/sourcebuster.min.js', array(), false, true);

  wp_enqueue_script('main', get_template_directory_uri().'/js/main.min.js', array(), false, true);
}
add_action( 'wp_enqueue_scripts', 'lirax_scripts' );

function roboto_font_url() {
	$font_url = '';
	$query_args = array(
		'family' => urlencode( 'Roboto:100,300,400,500' ),
		'subset' => urlencode( 'latin,cyrillic' ),
	);
	$font_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	return $font_url;
}
            


// <!--[END] загружаем css во фронтэнде сайта



if ( ! function_exists( 'lirax_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function lirax_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on lirax-theme, use a find and replace
	 * to change 'lirax-theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'lirax-theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Разделы', 'lirax-theme' ),
		'main_menu' => esc_html__( 'Главное меню', 'lirax-theme' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'lirax_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'lirax_theme_setup' );



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function lirax_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'lirax_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'lirax_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function lirax_theme_widgets_init() {
	
	register_sidebar( array(
		'name'          => esc_html__( 'Правая колонка', 'lirax-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'lirax-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Вверхний блок', 'lirax-theme' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'lirax-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Подменю', 'lirax-theme' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Подменю разделов', 'lirax-theme' ),
		'before_widget' => '<div id="nav_menu_3"><a href="#%1$s" class="visible-xx menu-btn" data-action="showOpen">Меню</a>',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="hidden">',
		'after_title'   => '</div>',
	) );

  register_sidebar(array(
     'name'         => esc_html__( 'Футер', 'lirax-theme' ),
     'id'           => "footer",
     'description'  => esc_html__( 'Место под виджеты в футере', 'lirax-theme' ),
     'class' => '',
     'before_widget' => '<div class="cell">',
     'after_widget' => '</div>',
     'before_title' => '<div class="title">',
     'after_title' => '</div>'
  ));
  register_sidebar(array(
     'name'         => esc_html__( 'Форум', 'lirax-theme' ),
     'id'           => "forum",
     'description'  => esc_html__( 'Авторизация на форуме', 'lirax-theme' ),
     'class' => '',
     'before_widget' => '',
     'after_widget' => '',
     'before_title' => '',
     'after_title' => ''
  ));
}
add_action( 'widgets_init', 'lirax_theme_widgets_init' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';






/*
 * "Хлебные крошки" для WordPress
 * автор: Dimox
 * версия: 2015.09.14
 * лицензия: MIT
*/
function dimox_breadcrumbs() {

  /* === ОПЦИИ === */
  $text['home'] = '<i class="fa fa-home" aria-hidden="true"></i>'; // текст ссылки "Главная"
  $text['category'] = '%s'; // текст для страницы рубрики
  $text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
  $text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
  $text['author'] = 'Статьи автора %s'; // текст для страницы автора
  $text['404'] = 'Ошибка 404'; // текст для страницы 404
  $text['page'] = 'Страница %s'; // текст 'Страница N'
  $text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

  $wrap_before = '<div class="breadcrumbs">'; // открывающий тег обертки
  $wrap_after = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
  $sep = '›'; // разделитель между "крошками"
  $sep_before = '<span class="sep">'; // тег перед разделителем
  $sep_after = '</span>'; // тег после разделителя
  $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
  $show_on_home = 1; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
  $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
  $before = '<span class="current">'; // тег перед текущей "крошкой"
  $after = '</span>'; // тег после текущей "крошки"
  /* === КОНЕЦ ОПЦИЙ === */

  global $post;
  $home_link = home_url('articles/');
  $link_before = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
  $link_after = '</span>';
  $link_attr = ' itemprop="url"';
  $link_in_before = '<span itemprop="title">';
  $link_in_after = '</span>';
  $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
  $frontpage_id = get_option('page_on_front');
  $parent_id = $post->post_parent;
  $sep = ' ' . $sep_before . $sep . $sep_after . ' ';

  if (is_home() || is_front_page()) {

    if ($show_on_home) echo $wrap_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>' . $wrap_after;

  } else {

    echo $wrap_before;
    if ($show_home_link) echo sprintf($link, $home_link, $text['home']);

    if ( is_category() ) {
      $cat = get_category(get_query_var('cat'), false);
      if ($cat->parent != 0) {
        $cats = get_category_parents($cat->parent, TRUE, $sep);
        $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        if ($show_home_link) echo $sep;
        echo $cats;
      }
      if ( get_query_var('paged') ) {
        $cat = $cat->cat_ID;
        echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
      }

    } elseif ( is_search() ) {
      if (have_posts()) {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
      } else {
        if ($show_home_link) echo $sep;
        echo $before . sprintf($text['search'], get_search_query()) . $after;
      }

    } elseif ( is_day() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
      echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
      if ($show_current) echo $sep . $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
      if ($show_current) echo $sep . $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ($show_home_link) echo $sep;
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
        if ($show_current) echo $sep . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $sep);
        if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
        if ( get_query_var('cpage') ) {
          echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
        } else {
          if ($show_current) echo $before . get_the_title() . $after;
        }
      }

    // custom post type
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      if ( get_query_var('paged') ) {
        echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . $post_type->label . $after;
      }

    } elseif ( is_attachment() ) {
      if ($show_home_link) echo $sep;
      $parent = get_post($parent_id);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      if ($cat) {
        $cats = get_category_parents($cat, TRUE, $sep);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
      }
      printf($link, get_permalink($parent), $parent->post_title);
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && !$parent_id ) {
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && $parent_id ) {
      if ($show_home_link) echo $sep;
      if ($parent_id != $frontpage_id) {
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          if ($parent_id != $frontpage_id) {
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
          }
          $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo $breadcrumbs[$i];
          if ($i != count($breadcrumbs)-1) echo $sep;
        }
      }
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      if ( get_query_var('paged') ) {
        $tag_id = get_queried_object_id();
        $tag = get_tag($tag_id);
        echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
      }

    } elseif ( is_author() ) {
      global $author;
      $author = get_userdata($author);
      if ( get_query_var('paged') ) {
        if ($show_home_link) echo $sep;
        echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
      }

    } elseif ( is_404() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . $text['404'] . $after;

    } elseif ( has_post_format() && !is_singular() ) {
      if ($show_home_link) echo $sep;
      echo get_post_format_string( get_post_format() );
    }

    echo $wrap_after;

  }
} // end of dimox_breadcrumbs()


// add a new logo to the login page
function wptutsplus_login_logo() { ?>
    <style type="text/css">
        .login #login h1 a {
            background-image: url( <?php echo 'wp-admin/images/w-logo-blue.png'; ?> );
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'wptutsplus_login_logo' );


// add_action( 'init', 'blockusers_init' );
//   function blockusers_init() {
//   if ( is_admin() && ! current_user_can( 'administrator' ) &&
//   ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
//   wp_redirect( home_url() );
//   exit;
//   }
// }

// /* Отключаем админ панель для всех, кроме администраторов. */
if (!current_user_can('administrator')):
  show_admin_bar(false);
endif;
 
//[this_tamplate]
function this_tamplate(){
	return get_template_directory_uri();
}
add_shortcode( 'this_tamplate', 'this_tamplate' );


function make_recode($atts, $content){
  return esc_html($content);
}
add_shortcode( 'recode', 'make_recode' );

add_filter('widget_text', 'do_shortcode');

// отключаем автоформатирование
remove_filter('the_content', 'wpautop');


//Изменение длины обрезаемого текста
function new_excerpt_length($length) {
  return 32;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Удаление конструкции [...] на конце.
add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more($more) {
  global $post;
  return '... <a href="'. get_permalink($post->ID) . '" class="readmore">далее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>';
}

// удаление граватара
function my_bro_gravatar_desabler( $url, $id_or_email, $args){
    return $args['default'];
}
add_filter('get_avatar_url', 'my_bro_gravatar_desabler',10 ,3);


/**
* Load Enqueued Scripts in the Footer
*
* Automatically move JavaScript code to page footer, speeding up page loading time.
*/
function footer_enqueue_scripts(){
   remove_action('wp_head','wp_print_scripts');
    remove_action('wp_head','wp_print_head_scripts',9);
    remove_action('wp_head','wp_enqueue_scripts',1);
    add_action('wp_footer','wp_print_scripts',5);
    add_action('wp_footer','wp_enqueue_scripts',5);
    add_action('wp_footer','wp_print_head_scripts',5);
}
//add_action('after_setup_theme','footer_enqueue_scripts');


/*
* Реализация функции для получения курсов валют НБУ,
* используемых для начисления налогов и сборов 
* с использованием алгоритма кеширования
* 
* В папке с данным файлом обязательно должна присутствовать папка 'cache'
* с правами доступа 0777
*/
function getRatesXml($ratesDate = null){
    if(!$ratesDate) $ratesDate=time();
    $requestDate = mktime(0, 0, 0, date('m', $ratesDate), date('d', $ratesDate), date('Y', $ratesDate));    
    
    /* удаляем вчерашний файл */
    $oldDate = mktime(0, 0, 0, date('m', $ratesDate), date('d', $ratesDate)-1, date('Y', $ratesDate));
    $oldFile='wp-content/cache/currency/'.date('Y-m-d', $oldDate).'.json';
    if (file_exists($oldFile)) unlink($oldFile);
    
    $cacheFile='wp-content/cache/currency/'.date('Y-m-d', $requestDate).'.json';
    /* проверяем наличие файла */
    if (file_exists($cacheFile)){
      /* если последний запрос был меньше 10 минут назад, возвращаем его данные */
      if(time() - filemtime($cacheFile) < 3600){
        return file_get_contents($cacheFile);
      }
    }
         
    /* запрос на получение данных */
    $responceXml = file_get_contents("https://api.minfin.com.ua/mb/cf2734d0655ecbed6f4b78ab0656a4888a8f10ee/");
    $dateFromRequest = get_lastdate_currency($responceXml);
    if(file_exists($cacheFile))
      $dateFromLast = get_lastdate_currency(file_get_contents($cacheFile));
    else
      file_put_contents($cacheFile, $responceXml);
      return $responceXml;


    if ($dateFromRequest != $dateFromLast) {
      if(file_exists($cacheFile)) unlink($cacheFile);
      file_put_contents($cacheFile, $responceXml);
      return $responceXml;
    } else {
      return file_get_contents($cacheFile);
    }  
}

/* Очищаем json */
function json_clean_decode($json, $assoc = false, $depth = 512, $options = 0) { 
  $json = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t](//).*)#", '', $json); 

  if(version_compare(phpversion(), '5.4.0', '>=')) { 
      $json = json_decode($json, $assoc, $depth, $options); 
  } 
  elseif(version_compare(phpversion(), '5.3.0', '>=')) { 
      $json = json_decode($json, $assoc, $depth); 
  } 
  else { 
      $json = json_decode($json, $assoc); 
  } 
  return $json; 
}

/* Получаем курс валюты евро */
function get_euro_currency(){
  $currency = json_clean_decode(getRatesXml());
  if (!isset($currency))
    return null;

  foreach ($currency as $obj)
    if ($obj->currency == 'eur' && $obj->status != 'deleted') return $obj->ask;
}

/* Добавляем шорткод для получения курса валют */
add_shortcode( 'get_euro', 'get_euro_currency');

/* Получаем последнюю дату обновления евро с jsona*/
function get_lastdate_currency($arg){
  $array = json_clean_decode($arg);
  foreach ($array as $obj) {
    if ($obj->currency == 'eur' && $obj->status != 'deleted') return $obj->date;
  }
}


function mayak_filter_image($content) {
$ar_mk  = '!<img (.*?) width="(.*?)" height="(.*?)" (.*?)/>!si';
$br_mk = '<span itemprop="image" itemscope itemtype="https://schema.org/ImageObject"><img itemprop="url image" \\1 width="\\2" height="\\3" \\4/><meta itemprop="width" content="\\2"><meta itemprop="height" content="\\3"></span>';
$content = preg_replace($ar_mk, $br_mk, $content);  
  return $content;     
}
add_filter('the_content', 'mayak_filter_image');

register_sidebar( array(
        'name' => __( 'WPML в шапке', '' ),
        'id' => 'top-area',
        'description' => __( 'Больше нечего сюда не вставлять', '' ),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ) );