<div class="popup popup-cristmas">
	<div class="header">
		<div class="pic"><i class="fa fa-gift" aria-hidden="true"></i></div>
		<div class="title">Поздравляем Вас с Новым 2017 Годом!</div>
	</div>
	<div class="content">
		<p>Финансового роста и процветания в новом году!<br>Достойных, продуктивных рабочих связей и контактов. <br> Успехов в любых начинаниях и проектах.</p>
		<div class="action-block">
			<a href="javascript:;" onclick="$.fancybox.next()" class="btn btn-yellow">Забрать свой подарок</a><br>
			<a href="javascript:;" onclick="disableSpecialPopup('newYearPopup')" class="small-text">Спасибо, я уже получил свой Подарок ;)</a>
		</div>
	</div>
	<!-- <div class="footer">
		<img src="/wp-content/themes/lirax-theme/tpl/newYearPopup-bg.png" alt="image">
	</div> -->
</div>


