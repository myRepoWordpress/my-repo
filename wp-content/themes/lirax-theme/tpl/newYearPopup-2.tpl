<div class="popup popup-cristmas popup-step2">
	<div class="header">
		<div class="pic"><i class="fa fa-gift" aria-hidden="true"></i></div>
		<div class="title">Вы уже являетесь нашим клиентом?</div>
	</div>
	<div class="content">
		<div class="action-block">
			<a href="javascript:;" onclick="getPresent('client', 'newYearPopup')" class="btn btn-yellow">Да, конечно!</a> <br>
			<a href="javascript:;" onclick="getPresent('guest', 'newYearPopup')" class="btn btn-link">Нет, но тоже хочу подарок!</a>
		</div>
	</div>
</div>
