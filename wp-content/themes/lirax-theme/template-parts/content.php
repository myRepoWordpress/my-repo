<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package lirax-theme
 */
$meta = new stdClass;
foreach( get_post_meta( get_the_ID() ) as $k => $v )
    $meta->$k = $v[0];
?>


	<article id="post-<?php the_ID();?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php lirax_theme_posted_on(); ?>
					
				<?php if($meta->youtube) : ?>
            		<span class="entry-video">
                	<a href="<?=$meta->youtube?>" class="fancybox-media" rel="post-<?php the_ID();?>">
                   	<i class="fa fa-youtube-play" aria-hidden="true"></i>смотреть видео
                    </a>
            		</span>
            	<?php endif; ?>
				
				<?php if ( is_single() ) : ?>
					<div class="social-btns">
						<?php 
						/*
						<script type="text/javascript">(function(w,doc) {
						if (!w.__utlWdgt ) {
						    w.__utlWdgt = true;
						    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
						    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
						    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
						    var h=d[g]('body')[0];
						    h.appendChild(s);
						}})(window,document);
						</script>
						<div data-background-alpha="0.0" data-orientation="horizontal" data-text-color="#000000" data-share-shape="SHARP" data-buttons-color="#FFFFFF" data-sn-ids="fb.gp." data-counter-background-color="#ffffff" data-share-size="LARGE" data-background-color="#ffffff" data-mobile-sn-ids="fb.gp." data-preview-mobile="false" data-pid="1673069" data-counter-background-alpha="1.0" data-mode="like" data-following-enable="false" data-exclude-show-more="false" data-like-text-enable="true" data-selection-enable="false" data-mobile-view="false" data-icon-color="#ffffff" class="uptolike-buttons" ></div>
						*/
						?>
            		</div>
            	<?php endif; ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
		</header><!-- .entry-header -->
		

		<div class="entry-content">
			<?php if ( !is_single() ) : ?>
				<div class="row">
					<div class="span-xs-12">
						<div class="post-image">
							<?php if(get_the_post_thumbnail($page->ID)) : ?>
								<?=get_the_post_thumbnail($page->ID)?>
							<?php else : ?>
							    <img src="<?=get_template_directory_uri()?>/images/default.jpg">
							<?php endif; ?>
						</div>
						<?php
							the_content( sprintf(
								/* translators: %s: Name of current post. */
								wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'lirax-theme' ), array( 'span' => array( 'class' => array()))),
								 the_title( '<span class="hidden">"', '"</span>', false )
							));
						?>
					</div>
				</div>
			

			<?php else:
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'lirax-theme' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				), true);

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'lirax-theme' ),
					'after'  => '</div>',
				) );

			endif; ?>
		</div><!-- .entry-content -->
		
		<?php /* if ( is_single() ) : ?>
			<div class="entry-comments">
				<div id="hypercomments_widget"></div>
				<script type="text/javascript">
				_hcwp = window._hcwp || [];
				_hcwp.push({widget:"Stream", widget_id: 79638});
				(function() {
				if("HC_LOAD_INIT" in window)return;
				HC_LOAD_INIT = true;
				var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
				var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
				hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/79638/"+lang+"/widget.js";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(hcc, s.nextSibling);
				})();
				</script>
			</div>
		<?php endif; */ ?>

		<footer class="entry-footer">
			<?php lirax_theme_entry_footer(); ?>
		</footer><!-- .entry-footer -->

	</article><!-- #post-## -->

