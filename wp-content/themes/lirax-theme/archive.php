<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package lirax-theme
 */

get_header(); 
?>

	<div class="sidebar-2-block">
		<div class="container">
			<div class="row">
				<div class="span-xx-12">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="breadcrumbs-block">
		<div class="container">
			<div class="row">
				<div class="span-xx-12">
					<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container" data-page-type="arhive">

		<?php
		if ( have_posts() ) : ?>
			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->
			
			<div class="row">
				
						<div class="span-md-8 span-lg-9">

						<?php 
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								 /* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.*/
								 
								get_template_part( 'template-parts/content', get_post_format() );

							endwhile;

							the_posts_navigation();
						?>
				</div>
				<?php get_sidebar(); ?>
			</div>

		<?php else :
			
			get_template_part( 'template-parts/content', 'none' );
		
		endif; ?>
	
	</div>
	
<?php
get_footer();
