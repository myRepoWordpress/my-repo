<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package lirax-theme
 */

get_header(); ?>
	<div class="sidebar-2-block">
		<div class="container">
			<div class="row">
				<div class="span-xx-12">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="breadcrumbs-block">
		<div class="container">
			<div class="row">
				<div class="span-xx-12">
					<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>


	<div class="container" data-page-type="index">

		<div class="row">
			<?php if ( have_posts() ) :
				if ( is_home() && ! is_front_page() ) : ?>
				<div class="span-xx-12">
					<header>
						<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</header>
				</div>
				<?php
				endif; 
			endif; ?>
			<div class="span-md-8 span-lg-9">
				<?php
					if ( have_posts() ) :

						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_format() );

						endwhile;

						the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

				endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>


<?php
get_footer();
