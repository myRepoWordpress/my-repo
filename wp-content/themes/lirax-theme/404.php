<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package lirax-theme
 */
$currenturl = $_SERVER['REQUEST_URI'];
$emptyPage = '/404';
if ($currenturl != $emptyPage){
	//wp_redirect( 'http://lirax.com.ua/'.$emptyPage, 301 ); exit; 
}
get_header(); ?>

	<div class="empty-page">
		<div class="container">

			<h1 class="page-title">Ой! Страница не найдена :(</h1>
			<div class="page-content">Похоже, ничего не найдено. Может быть, попробовать одну из ссылок справа?</div>
			<span class="links-block">
			<a href="/lirax-kyivstar/" class="links link-1">Многоканальный мобильный номер</a>
			<a href="/" class="links link-2">IP телефония</a>
			<a href="/crm/" class="links link-3">CRM</a>
			<a href="/callback/" class="links link-4">CallBack</a>
			<a href="/articles/kb/" class="links link-5">База знаний</a>
			<a href="/calltracking/" class="links link-6">CallTracking</a>
			</span>
			<a href="/" class="links link-home hidden-xx"></a>
			<img src="<?=get_template_directory_uri()?>/images/404b.jpg" class="hidden-xs hidden-xx" alt="404">
			<img src="<?=get_template_directory_uri()?>/images/404b-576.jpg" class="visible-xs" alt="404">
		</div>
	</div>

	<!-- <div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php //esc_html_e( 'Oops! That page can&rsquo;t be found.', 'lirax-theme' ); ?></h1>
				</header>

				<div class="page-content">
					<p><?php //esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'lirax-theme' ); ?></p>

					<?php
						//get_search_form();

						//the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						//if ( lirax_theme_categorized_blog() ) :
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php //esc_html_e( 'Most Used Categories', 'lirax-theme' ); ?></h2>
						<ul>
						<?php
							//wp_list_categories( array(
							// 	'orderby'    => 'count',
							// 	'order'      => 'DESC',
							// 	'show_count' => 1,
							// 	'title_li'   => '',
							// 	'number'     => 10,
							// ) );
						?>
						</ul>
					</div>

					<?php
						//endif;

						/* translators: %1$s: smiley */
						//$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'lirax-theme' ), convert_smilies( ':)' ) ) . '</p>';
						//the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

						//the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div>
			</section>
		
			</div>
		</main>
	</div> -->

<?php
get_footer();
