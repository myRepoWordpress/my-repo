<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lirax-theme
 */

get_header(); 
?>
	
	<div class="sidebar-2-block">
		<div class="container">
			<div class="row">
				<div class="span-xx-12">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="breadcrumbs-block">
		<div class="container">
			<div class="row">
				<div class="span-xx-12">
					<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="container" data-page-type="single">
		<div class="row">
			<?php if ( ! is_active_sidebar( 'sidebar-1' ) ) : ?>
				<div class="span-xx-12">
			<?php else : ?>
				<div class="span-md-8 span-lg-9">
			<?php endif; ?>
			
				<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', get_post_format() );

						//the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
				?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php

get_footer();
