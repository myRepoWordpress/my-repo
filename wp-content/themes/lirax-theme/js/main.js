$(document).ready(function() {
    var formDataSerialize; 

    var homeTitleDesc = [
        'Помогает управлять обработкой всех обращений ваших клиентов', 
        'Привлекает новых клиентов, увеличивает объемы продаж',
        'Удерживает клиентов, укрепляет имидж компании',
        'Снижает издержки на обработку исходящих вызовов',
        'Повышает качество обслуживания клиентов'
    ];

    Array.prototype.shuffle = function(b){
        var i = this.length, j, t;
        while(i){
            j = Math.floor( ( i-- ) * Math.random() );
            t = b && typeof this[i].shuffle!=='undefined' ? this[i].shuffle() : this[i];
            this[i] = this[j];
            this[j] = t;
        }
        return this;
    };

    function miniSlider(array){
        var shuffleArray = array.shuffle();
        var arraySize = array.length, i = 0;
        $('#miniSlider').text(shuffleArray[i]);
        setInterval(function(){
            i++;
            $('#miniSlider').fadeOut(400, function(){
                $(this).text(shuffleArray[i]).fadeIn(400);
            });
            i >= arraySize ? i = 0 : i = i;
        }, 6000);
    }

    if($('*').is('#miniSlider')) {
        miniSlider(homeTitleDesc);
    }

    $('.lirax-custom-form').submit(function(e){
        e.preventDefault();
    })
    

    sbjs.init({ 
        callback: function() { 
            $("[name='utm_source']").val(sbjs.get.first.src); 
            $("[name='utm_medium']").val(sbjs.get.first.mdm); 
            $("[name='utm_campaign']").val(sbjs.get.first.cmp); 
            $("[name='utm_term']").val(sbjs.get.first.trm); 
        } 
    });

    var accrordionIcons = {
      header: "fa fa-plus-square-o",
      activeHeader: "fa fa-minus-square-o"
    };

    $('a.ex-link').click(function(){window.open($(this).data('link'));return false;});

    $( "#accordion" ).accordion({
        icons: accrordionIcons,
        collapsible: true,
        heightStyle: "content",
        activate: function(event, ui) {
            var target = $(ui.newHeader[0]);
            $.scrollTo(target, 800);
        }
    });

    $( "#toggle" ).button().on( "click", function() {
      if ($( "#accordion" ).accordion( "option", "icons" ) ) {
        $( "#accordion" ).accordion( "option", "icons", null );
      } else {
        $( "#accordion" ).accordion( "option", "icons", icons );
      }
    });



    $('.scrollTo a').click(function(e){
        var target =  $(e.currentTarget.hash);
        $('.open').removeClass('open');
        $(e.currentTarget).parent().addClass('current_page_item').siblings().removeClass('current_page_item');
        $.scrollTo(target, 800,{
            offset: {top:-60}
        });
        return false;
    })

    $('.scrollToID').click(function(e){
        var target =  $(e.currentTarget.hash);
        $.scrollTo(target, 800,{
            offset: {top:-10}
        });
        return false;
    })

    function EnableDisableSubmit(AnyField) {

        var btnResult = AnyField.closest('form').find('[type=submit]');
        var testInputVal = AnyField.val();
        
        var VRegExp = new RegExp(/^(380)+[0-9]{9}$/);
        var VResult = VRegExp.test(testInputVal);
        
        if(VResult){
            btnResult.prop('disabled', false);
        } else {
            btnResult.prop('disabled', true);
        }
    }

    function formDataUpdate(e){
        var formData = e.serializeArray(), 
            dataCalc = e.attr('data-calc'),
            i = {};
        $(formData).each(function(e, t) {
            i[t.name] = t.value
        });
        
        var mobileKyivstarDefault = 0, mobileKyivstarLinesPrice = 0, liraxWorkPlacesPrice = 50, calltrackingPrice = 0, 
            liraxGSMPrice = 100, conuntLinesPrice = 0, countNumbersPrice = 0,
            cowp = parseInt(i.countOfWorkPlaces),
            coc = parseInt(i.countOfLines),
            con = parseInt(i.countOfNumbers),
            cog = parseInt(i.countOfGSM),
            co = i.calltrackingOption,
            wo = i.widgetOption,
            crm = i.crmOption,
            mko = i.mobileKyivstar,
            ckl = parseInt(i.countOfKyivstarlines),
            cs = parseInt(i.calcSum);

            countOfLinesDefault = cowp * 2;
            countOfNumbersDefault = Math.ceil(cowp / 2);


            if (co == 'on'){
                calltrackingPrice = 220
                $('.calcLineCT').removeClass('disabled-calc-line');
            } else {
                $('.calcLineCT').addClass('disabled-calc-line');
            }

            if (crm == 'on'){
                liraxWorkPlacesPrice = 160;
                $('.calcLineCRM').removeClass('disabled-calc-line');
            } else {
                $('.calcLineCRM').addClass('disabled-calc-line');
            }

            wo == 'on' ? $('.calcLineWidget').removeClass('disabled-calc-line') : $('.calcLineWidget').addClass('disabled-calc-line');
            coc > 0 ? $('.calcSIPLines').removeClass('disabled-calc-line') : $('.calcSIPLines').addClass('disabled-calc-line');
            con > 0 ? $('.calcDopNumbers').removeClass('disabled-calc-line') : $('.calcDopNumbers').addClass('disabled-calc-line');
            cog > 0 ? $('.calcGSM').removeClass('disabled-calc-line') : $('.calcGSM').addClass('disabled-calc-line');


            $('#calcDesc').text(400);
            if (mko == 'on'){
                mobileKyivstarDefault = 207;
                countOfLinesDefault = countOfLinesDefault - ckl - 1;
                countOfNumbersDefault = countOfNumbersDefault - 1;
                mobileKyivstarLinesPrice = ckl * 49;
                $('#calcDesc').text(600);
                $('.calcKyivstar').removeClass('disabled-calc-line');
                ckl > 0 ? $('.calcLineKyivstar').removeClass('disabled-calc-line') : $('.calcLineKyivstar').addClass('disabled-calc-line');

            } else {
                $('#countOfKyivstarlines').val(0);
                $('.calcKyivstar, .calcLineKyivstar').addClass('disabled-calc-line');

            }

            liraxWorkPlacesPrice*=cowp;
            liraxGSMPrice*=cog;
            if (coc > countOfLinesDefault)
                conuntLinesPrice = (coc - countOfLinesDefault) * 15

            if (con > countOfNumbersDefault)
                countNumbersPrice = (con - countOfNumbersDefault) * 20


            if (countOfLinesDefault > 0)
                $('#countLinesDefault').html(' ' + countOfLinesDefault + ' ');
            else
                $('#countLinesDefault').html('&nbsp;');


            if(countOfNumbersDefault > 0)
                $('#countDopNumbersDefault').html(' ' + countOfNumbersDefault + ' ' );
            else
                $('#countDopNumbersDefault').html('&nbsp;');

            $('.calcSIPLines')



            calcSumLirax = mobileKyivstarDefault + mobileKyivstarLinesPrice + liraxWorkPlacesPrice + calltrackingPrice + liraxGSMPrice + conuntLinesPrice + countNumbersPrice;
            
            
            formDataSerialize = 'Количество рабочих мест: ' + cowp + '\n' +
            'Количество дополнительных SIP номеро: ' + con + '\n' +
            'Количество линий для доп. номеров: ' + coc + '\n' +
            'Аренда GSM шлюзов: ' + cog + '\n';


            if (mko == 'on'){
                datamoKyivstar = 'Многоканальный мобильный номер Киевстар \n' + 
                'Количество дополнительных линий Киевстар: ' + ckl + '\n';
                formDataSerialize = datamoKyivstar + formDataSerialize;
            }

            if (co == 'on')
                formDataSerialize+= 'Колтрекинг \n';

            if (crm == 'on')
                formDataSerialize+= 'CRM \n';

            if (wo == 'on')
                formDataSerialize+= 'Виджет \n';

            formDataSerialize+= 'Итого: ' + calcSumLirax + ' грн/мес \n';

            mko == 'on' ? formDataSerialize+= 'Подключение 600 грн единоразово' : formDataSerialize+= 'Подключение 400 грн единоразово';


            // console.log(formDataSerialize);

            $('#calcSumInput').val(calcSumLirax);
            $('#calcSumLirax').text(calcSumLirax); 

           
            // console.log('Разница: ' + (calcSumLirax - cs));

    }
   
    if ($('form').is('#calcLiraxForm')) formDataUpdate($('#calcLiraxForm'));

    var optionsPhone =  {
      onKeyPress: function(cep, event, currentField, options){
        EnableDisableSubmit(currentField);
      }
    };

    var optionsNumber = {
        selectOnFocus: true,
        onKeyPress: function(cep, event, currentField, options){
            var minInputValue = parseInt(currentField.attr('min-value')),
                inputValue = parseInt(cep, 10);
            inputValue > minInputValue ? currentField.val(inputValue) : currentField.val(minInputValue);
            formDataUpdate(currentField.closest('form'));
        }
    }

    // github.com/igorescobar/jQuery-Mask-Plugin
    $('.phone-mask').mask('380000000000', optionsPhone);
    
    $('.number-mask').mask('00', optionsNumber);

    $('[data-change]').change(function(e){
        var target = $(e.currentTarget),
            dataCase = target.attr('data-change');
        switch(dataCase){
            case "updateForm":
                formDataUpdate(target.closest('form'));
                break;
        }
    })

    

    var t = "undefined",
        a = $(".navigator"),
        globalDataSubject;
    if ($(window).scroll(function() {
        $(this).scrollTop() > 152 ? a.addClass("fixed") : $(this).scrollTop() <= 152 && a.removeClass("fixed")
    }), 

    

    window.devicePixelRatio > 1) {
        var dw = $(document).width();
        if (dw >= 480){
            setTimeout(function(){
                var o = $(".retina-ready");
                o.each(function(e) {
                    var w = this.width,
                        t = $(this).attr("src"),
                        a = t.replace(/\.(gif|png|jpg)$/gi, "@2x.\$1");
                    $(this).attr("src", a);
                    this.width = w;
                })
            }, 1000);
        }
    }

    function $_GET(key) {
        var s = window.location.search;
        s = s.match(new RegExp(key + '=([^&=]+)'));
        return s ? s[1] : false;
    }

    var showMe = $_GET('showMe');
    if(showMe == 'all') {
        $('.showMe').removeClass('hidden');
    }

    function modalIconChange(filename){
        url = $("#modalIcon").attr("src");
        name = url.substring(url.lastIndexOf('/')+1,url.length).replace(/\.[^.]+$/, "");
        if(name.search('@2x') != -1) {
           name = name.replace('@2x', '');
        }
        url = url.replace(name, filename);
        $("#modalIcon").attr("src", url);
    }


    $(document).click(function(event) {
        if ($(event.target).closest(".open").length) return;
        $('.open').removeClass('open');
        $('body').removeClass('data-active');
        $('#shudowWrap').remove();
        event.stopPropagation();
    });
     


    $("[data-action]").click(function(e) {
            e.preventDefault();
            var a = $(e.currentTarget),
                o = a.attr("data-action"),
                n = $(a.attr("href"));
            switch (t = "undefined", calcData = {}, o) {
                case "plusMinusDig":
                    var dataForm = a.closest('form');
                        dataStep = parseInt(a.attr('data-step')),
                        dataInput = a.parent().siblings().find('input'),
                        dataInputValue = parseInt(dataInput.val());
                        dataInputMin = parseInt(dataInput.attr('min-value'));
                        newDataValue = dataInputValue + dataStep;
                        if (newDataValue > dataInputMin){
                            dataInput.val(newDataValue);
                        } else {
                             dataInput.val(dataInputMin);
                        }
                        formDataUpdate(dataForm);
                    break
                case "activeTab":
                    var allControls = a.closest('.tabsControls').find('[data-action="activeTab"]'),
                        allTabs = n.closest('.tabs').find('.tab');
                    allControls.removeClass('active');
                    allTabs.removeClass('active');
                    a.addClass('active');
                    n.addClass('active');
                    break;
                case "specToggle":
                    e.preventDefault();
                    var w = a.closest('.toggle-wrap'),
                        p = w.parent(),
                        tb = w.find('.toggle-block'),
                        to = p.find('.toggle-open'),
                        ae = p.find('.active'),
                        ac = ae.length,
                        speed = 300,
                        tt = a.next('.toggle-text').text();
                        tb.stop(!0);

                        if(!a.hasClass('active')){
                            if (ac == 0){
                                a.addClass('active');
                                tb.html('<p>' + tt + '</p>').addClass('toggle-open').slideDown(speed);
                            } else {
                                to.slideUp(speed, function(){
                                    to.removeClass('toggle-open');
                                    ae.removeClass('active');
                                    a.addClass('active');
                                    tb.html('<p>' + tt + '</p>').addClass('toggle-open').slideDown(speed);
                                });
                            }
                        } else {
                            to.slideUp(speed, function(){
                                a.removeClass('active');
                            });
                        }
                    break;
                case "slideToggle":
                    var dReplace = a.attr("data-replace"),
                        dDefault = a.html();
                        n.stop(!0);
                        dDefault !== dReplace ? a.html(dReplace).attr("data-replace",dDefault) : null;
                        n.slideToggle("slow");
                    break;
                case "toggleClass":
                    n.toggleClass( "f-table-hide", 600, function(){
                        var dReplace = a.attr("data-replace"),
                        dDefault = a.html();
                        dDefault !== dReplace ? a.html(dReplace).attr("data-replace",dDefault) : null;
                    });
                    break;
                case "toggleClassShow":
                    n.removeClass( "block-hide", 600, function(){
                        a.addClass('disabled');
                        // dDefault = a.html();
                        // dDefault !== dReplace ? a.html(dReplace).attr("data-replace",dDefault) : null;
                    });
                    break;
                case "click":
                    if(n == '#liracrm-callme'){
                        // yaCounter36727460.reachGoal('clikbuttoncallwidget');
                        console.log('work');
                    }

                    n.click();
                    break;
                case "showSmallForm":
                    a.hide();
                    a.next('.calc-small-form').removeClass('hidden');
                    break;
                case "showOpen":
                    var parentBlock = $(this).parent();
                    parentBlock.hasClass('open') ? parentBlock.removeClass('open') : parentBlock.addClass('open');
                    //$('body').hasClass('data-active') ? $('body').removeClass('data-active') : $('body').addClass('data-active');
                    break;
                case "slideMenu":
                    var parentBlock = $(this).parent();
                    parentBlock.hasClass('open') ? parentBlock.removeClass('open') : parentBlock.addClass('open');
                    $('body').prepend('<div id="shudowWrap"></div>');
                    $('body').hasClass('data-active') ? $('body').removeClass('data-active') : $('body').addClass('data-active');
                    break;
                case "close-this":
                    n.parent().removeClass('open');
                    break;
                case "showform":
                    var i = a.attr("data-form"), s, getForm, message = '';
                        
                        $(a.data('getform')) ? getForm = getFormDataString($(a.data('getform'))) : getForm = false;

                        if (getForm)
                            message = message + getForm;
                        
                        if (message)
                            $('#main_message').val(message);

                        globalDataSubject = a.attr("data-subject");
                        globalDataSubject == 'undefined' ? globalDataSubject = '' : globalDataSubject = globalDataSubject;
                        s = globalDataSubject;
                        $('#modal_subject_id').val(i);
                    switch (i) {
                        case "questionForManager":
                            modalIconChange('consultation-circle');
                            $("#modalTitle").html("Связаться со специалистом");
                            $("#modalSubTitle").html("Оставьте свои контакты, что бы наши специалисты смогли свзаться с Вами в течении 3-х минут ");
                            $("#modalSubject").val("Заявка на консультацию с менеджером " + s);
                            // ga('send', 'event', 'forms', 'view', 'Связаться со специалистом ' + s);
                            // yaCounter36727460.reachGoal('clikbuttonconsultation');
                            break;
                        case "questionForManagerUA":
                            modalIconChange('consultation-circle');
                            $("#modalTitle").html("Зв'язатися зі спеціалістом");
                            $("#modalSubTitle").html("Залиште свої контакти, щоб наші спеціалісти змогли зв'язатися з Вами протягом 3-х хвилин ");
                            $("#modalSubject").val("Замовлення на консультацію з менеджером " + s);
                            // ga('send', 'event', 'forms', 'view', 'Зв'язатися зі спеціалістом + s);
                            // yaCounter36727460.reachGoal('clikbuttonconsultation');
                            break;
                        case "ipBusinessPhone":
                            modalIconChange('connect-circle');
                            $("#modalTitle").html("Подключение IP Business Phone");
                            $("#modalSubTitle").html("Стоимость подключения всего 1,20 грн!<br>Доступные городские номера: Киев (044) и Львов (032)");
                            $("#modalSubject").val("Заявка на подключение IP Business Phone (0,25 грн/мин)");
                            // ga('send', 'event', 'forms', 'view', 'Заявка на подключение' + s);
                            // yaCounter36727460.reachGoal('clikbuttonnumber25');
                            break;
                        case "connectMeNow":
                            modalIconChange('connect-circle');
                            $("#modalTitle").html("Заявка на подключение");
                            $("#modalSubTitle").html("Оставьте свои контакты, что бы наши специалисты смогли свзаться с Вами для подключения к нашей системе");
                            $("#modalSubject").val("Заявка на подключение " + s);
                            // ga('send', 'event', 'forms', 'view', 'Заявка на подключение ' + s);
                            // yaCounter36727460.reachGoal('clikbuttontestdrive');
                            break;
                        case "connectMeNowUA":
                            modalIconChange('connect-circle');
                            $("#modalTitle").html("Замовлення на підключення");
                            $("#modalSubTitle").html("Залиште свої контакти, щоб наші спеціалісти змогли зв'язатися з Вами для підключення до нашої системи");
                            $("#modalSubject").val("Замовлення на підключення " + s);
                            // ga('send', 'event', 'forms', 'view', 'Замовлення на підключення ' + s);
                            // yaCounter36727460.reachGoal('clikbuttontestdrive');
                            break;
                        case "onlinePresentation":
                            modalIconChange('online-presentation-circle');
                            $("#modalTitle").html("Заказать On-line демонтрацию");
                            $("#modalSubTitle").html("По завершению презентации Вы получите демо доступ к нашей системе");
                            $("#modalSubject").val("Заявка на демонтрацию on-line " + s);
                            // ga('send', 'event', 'forms', 'view', 'Заказать On-line демонтрацию ' + s);
                            // yaCounter36727460.reachGoal('clikbuttondemonstration');
                            break;
                        case "onlinePresentationUA":
                            modalIconChange('online-presentation-circle');
                            $("#modalTitle").html("Замовити On-line демонстрацію");
                            $("#modalSubTitle").html("По завершенню презентації Ви отримаєте демо доступ до нашої системи");
                            $("#modalSubject").val("Замовлення на демонтрацію on-line " + s);
                            // ga('send', 'event', 'forms', 'view', 'Замовити On-line демонстрацію ' + s);
                            // yaCounter36727460.reachGoal('clikbuttondemonstration');
                            break;
                        case "calculateMe":
                            modalIconChange('calculate-circle');
                            $("#modalTitle").html("Заказать расчет стоимости");
                            $("#modalSubTitle").html("Мы с радостью поможем просчитать максимально выгодный для Вас тарифный план с учетом особенностей Вашего бизнеса");
                            $("#modalSubject").val("Заявка на индивидуальный просчет " + s);
                            // ga('send', 'event', 'forms', 'view', 'Заказать расчет стоимости ' + s);
                            break;
                        case "orderCalltracking":
                            modalIconChange('connect-circle');
                            $("#modalTitle").html("Оформить заказ");
                            $("#modalSubTitle").html("Оставьте свои контакты, что бы наши специалисты смогли свзаться с Вами для подключения к нашей системе");
                            $("#modalSubject").val("Заявка на подклчение " + s);
                            $("#modalForm").addClass("from-calc");
                            // ga('send', 'event', 'forms', 'view', 'Оформить заказ ' + s);
                            break;
                        case "orderCalltrackingUA":
                            modalIconChange('connect-circle');
                            $("#modalTitle").html("Оформити замовлення");
                            $("#modalSubTitle").html("Залиште свої контакти, щоб наші спеціалісти змогли зв'язатися з Вами для підключення до нашої системи");
                            $("#modalSubject").val("Заявка на підключення " + s);
                            $("#modalForm").addClass("from-calc");
                            // ga('send', 'event', 'forms', 'view', 'Оформити замовлення ' + s);
                            break;
                        case "orderVIPnumber":
                            modalIconChange('shop');
                            $("#modalTitle").html("Заказать узнаваемый номер");
                            $("#modalSubTitle").html("Оставьте свои контакты, что бы наши специалисты смогли свзаться с Вами. Специальная цена дейсвует для клиентов телефонии liraX (Тетраком).");
                            $("#modalSubject").val("Хочу купить " + s);
                            // ga('send', 'event', 'forms', 'view', 'Оформить заказ ' + s);
                            break;
                        case "orderVIPnumberUA":
                            modalIconChange('shop');
                            $("#modalTitle").html("Замовити впізнаваний номер");
                            $("#modalSubTitle").html("Оставьте свои контактыалишити свої контакти, щоб наші спеціалісти змогли зв'язатися з Вами. Спеціальна ціна діє для клієнтів телефонії liraX (Тетраком).");
                            $("#modalSubject").val("Хочу купити " + s);
                            // ga('send', 'event', 'forms', 'view', 'Оформити замовлення ' + s);
                            break;
                        case "orderGoods":
                            modalIconChange('shop');
                            $("#modalTitle").html("Подтверждение заказа");
                            $("#modalSubTitle").html("Оставьте свои контакты, что бы менеджер смог свзаться с Вами. Вы выбрали: <b>" + s + "</b>");
                            $("#modalSubject").val("Хочу купить " + s);
                            // ga('send', 'event', 'forms', 'view', 'Оформить заказ ' + s);
                            break;
                        case "orderGoodsUA":
                            modalIconChange('shop');
                            $("#modalTitle").html("Підтвердження замовлення");
                            $("#modalSubTitle").html("Залиште свої контакти, щоб менеджер зміг зв'язатися з Вами. Ви обрали: <b>" + s + "</b>");
                            $("#modalSubject").val("Хочу купити " + s);
                            // ga('send', 'event', 'forms', 'view', 'Оформити замовлення ' + s);
                            break;
                        default:
                            modalIconChange('connect-circle');
                            $("#modalTitle").html("Оставить заявку");
                            $("#modalSubTitle").text("Оставьте свои контакты, что бы наши специалисты смогли свзаться с Вами в ближайшее время. Спасибо!"); 
                            $("#modalSubject").val("Заявка с сайта");
                            // ga('send', 'event', 'forms', 'view', 'Оставить заявку ' + s);
                    }
                    r(n);
            }
    });


    $('.action-login').submit(function(e) {
        
        e.preventDefault();

        var target = $(e.currentTarget);
        var login = target.find('[name="login"]').val(),
            password = target.find('[name="password"]').val(),
            remember = target.find('[name="remember"]'),
            error,
            that = target.find('[type="submit"]');

        var url_data = 'login=' + login + '&password=' + password;

        $('.alert').remove();

        if (login.length == 0) {
            error = $('<div class="alert alert-warning">Введите свой Логин</div>').hide().fadeIn(500);
            that.before(error);
            return;
        }

        if (password.length == 0) {
            error = $('<div class="alert alert-warning">Введите пароль</div>').hide().fadeIn(500);
            that.before(error);
            return;
        }

        if (remember.is(":checked")) url_data = url_data + '&remember=1';


        $.ajax({
            url: 'https://lira.voip.com.ua/services/enter_json.php',
            data: url_data,
            dataType: 'json',
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            success: function(j) {
                if (j.message.ok == 1) {
                    window.location.replace("https://lira.voip.com.ua/main.php");
                } else if (j.message.ok == -1) {
                    error = $('<div class="alert alert-warning">Неверное имя пользователя или пароль</div>').hide().fadeIn(500);
                    that.before(error);
                }
            }
        });
    });

    $(".fancybox").fancybox({
        prevEffect  : 'fade',
        nextEffect  : 'fade',
        padding: 0,
    });

    $("a[href='#registerUrl']").fancybox({
        maxWidth    : 400,
        maxHeight   : 600,
        fitToView   : true,
        width       : '95%',
        height      : '95%',
        autoSize    : true
    });

    $(".fancybox-code").fancybox({
        maxWidth    : 800,
        maxHeight   : 600,
        fitToView   : true,
        width       : '70%',
        height      : '70%',
        autoSize    : true
    });
    var r = function(e) {
        $.fancybox(e, {
            padding: 0,
            maxWidth: 970,
            minWidth: 220,
            fitToView: !0,
            closeBtn: !1,
            afterClose: function() {
                $("#liraxModal .dataBlock").text('').siblings('form, .modal-example').show(), t = "undefined"
            }
        })
    };


    $(".fancybox-media").fancybox({
        padding: 0,
        helpers: {
            media: {}
        }
    }), 

    $(".close-fancybox").click(function(e) {
        e.preventDefault();
        $.fancybox.close();
    }),  

    $("#lookslider .slider").bxSlider({
        captions: !0,
        mode: "fade",
        pagerType: "short"
    }), 

    $("#thankSlider .slider").bxSlider({
        pager: !1
    }), 

    $("#sertifSlider .slider").bxSlider({
        pager: !1
    }), 

    $("#trustslider .slider").bxSlider({
        pager: !1,
        minSlides: 3,
        maxSlides: 5,
        slideWidth: 194,
        slideMargin: 0
    });

    
    $(".ajax-form_").on("submit", function(e) {        
        // e.preventDefault();
        // var target = $(e.currentTarget),
        //     goalCost,
        //     dataForm = target.attr('data-form'),
        //     modalSubjectID = $('#modal_subject_id').val();
        // var a, o = $(e.target),
        //     r = o.siblings(".dataBlock");
        // r.siblings('form, .modal-example').hide();
        // var n = o.serializeArray(), i = {}; // l = {};

        // $(n).each(function(e, t) {
        //     i[t.name] = t.value
        // });

        // switch(dataForm){
        //     case'xTremeForm':
        //         workPlaces = 'Количество рабочих мест: ' + $( '#liraxWorkPlaces' ).slider('value');
        //         $('input[name="equpment"]:checked').val() ? radioVal = 'Оборудование: купить' : radioVal = 'Оборудование: аренда';
        //         a = workPlaces + '\n' + radioVal;
        //         break;
        //     case'xLiteForm':
        //         workPlaces = 'Количество рабочих мест: ' + $( '#liraxWorkPlacesStandart' ).slider('value');
        //         workNumbers = 'Количество номеров: ' + $( '#liraxNumbersStandart' ).slider('value');
        //         a = workPlaces + '\n' + workNumbers;
        //     case'contacForm':
        //         a = i.Message;
        //         break;
        //     default:
        //         if (target.hasClass('from-calc')){
        //             ctUsers = 'Количество одновременных посетителей сайта: ' + $( '#countOfUsersSlider' ).slider('value');
        //             ctPlaces = 'Количество рабочих мест: ' + $( '#countOfWorkPlacesSlider' ).slider('value');
        //             ctCalls = 'Количество одновременных звонков: ' + $( '#countOfRingsSlider' ).slider('value');
        //             a = ctUsers + '\n' + ctPlaces + '\n' + ctCalls;
        //             target.removeClass('from-calc');
        //         } else if (target.hasClass('from-lirax-calc')) {
        //             a = formDataSerialize;
        //         } else {
        //             a = 'клиент ждет звонка';
        //         }
        // }
        // switch(globalDataSubject){
        //     case 'liraX':
        //         goalCost = 0;
        //         break;
        //     case 'CRM trial':
        //         goalCost = 0;
        //         break;
        //     case 'CRM':
        //         goalCost = 0;
        //         break;
        //     case 'Call Tracking':
        //         goalCost = 0;
        //         break;
        //     default:
        //         goalCost = 14;
        // }
        // console.log(modalSubjectID);
        // switch(modalSubjectID){
        //     case 'onlinePresentation':
        //         // yaCounter36727460.reachGoal('demonstration');
        //         break;
        //     case 'ipBusinessPhone':
        //         // yaCounter36727460.reachGoal('buynumber25');
        //         break;
        //     case 'questionForManager':
        //         // yaCounter36727460.reachGoal('consultation');
        //         break;
        //     case 'connectMeNow':
        //         // yaCounter36727460.reachGoal('testdrive');
        //         break;
        // }
        // var m = {
        //     contact_form_hash: "253815f490c34ae780548b358a10d2e3",
        //     contact_form_name: i.Name,
        //     contact_form_email: i.Email,
        //     contact_form_phone: i.Phone,
        //     contact_form_subject: i.Subject,
        //     contact_form_message: a,
        //     utm_source : sbjs.get.first.src,
        //     utm_medium : sbjs.get.first.mdm,
        //     utm_campaign : sbjs.get.first.cmp,
        //     utm_term : sbjs.get.first.trm,
        //     clientId : $("[name='clientId']").val(),
        //     trackingId :  $("[name='trackingId']").val()
        // };
        // $.ajax({
        //     type: "POST",
        //     url: "https://callme.voip.com.ua/contact_form.php",
        //     data: m,
        //     dataType: "json",
        //     success: function(e) {
        //         "error" == e.type ? r.html('<div class="pic"><i class="fa fa-times"></i> ' + e.text + "!</div>") : r.html('<div class="pic"><i class="fa fa-check"></i> Сообщение отправлено!</div>')
        //     },
        //     error: function(e, t) {
        //         r.html("Возникла ошибка: " + e.responseCode)
        //     }
        // })
    })

    $('#liraxWorkPlaces').slider({
        range: "min",
        value: 10,
        min: 2,
        max: 27,
        slide: function(n, e) {
            $('#countliraxWorkPlaces').text(e.value);
            var radioVal = $('input[name="equpment"]:checked').val();
            liraxCalculator(e.value, radioVal);
        }
    });

    $('#liraxWorkPlacesStandart').slider({
        range: "min",
        value: 5,
        min: 2,
        max: 15,
        slide: function(n, e) {
            var numbersCount = $('#liraxNumbersStandart').slider('value');
            liraxCalculatorStandart(e.value, numbersCount);
        }
    });

    $('#liraxNumbersStandart').slider({
        range: "min",
        value: 2,
        min: 1,
        max: 12,
        slide: function(n, e) {
            var workPlaces = $('#liraxWorkPlacesStandart').slider('value');
            liraxCalculatorStandart(workPlaces, e.value);
        }
    })

    $('input[name="equpment"]').change(function(){
        var radioVal = $('input[name="equpment"]:checked').val();
            workPlaces = $( '#liraxWorkPlaces' ).slider('value');
        liraxCalculator(workPlaces, radioVal);
    });


    var liraxCalculator = function(e,r) {
        var modemsCount = Math.floor(e / 3 + 3),
            routersCount = Math.ceil(modemsCount / 6),
            equipmentCost = modemsCount * 600 + routersCount * 1800,
            connectionPrice = modemsCount * 60 + 100,
            modemService, mouthCostBuy, modemArendaService,
            liraOfficePlace = 50;
            
        if (r == "true") {
            if (modemsCount <= 3) {
            modemService = 1500;
            } else if (modemsCount > 3 && modemsCount <= 7) {
                modemService = 1800;
            } else if (modemsCount > 7) {
                modemService = 1800 + 150 * (modemsCount - 7); 
            }
            modemService = modemService / 12;
        } else {
            modemService = 100 * modemsCount;
        }

        modemArendaService = 100 * modemsCount
        
        $('#liraxWorkPlaces .ui-slider-handle').html('<div class="workPlacesBlock">' + e + '</div>');

        mouthCostBuy = connectionPrice + liraOfficePlace * e + modemService;
        dayCostBuy = (mouthCostBuy / 30 / e).toFixed(2);
        mounthCostBuy = (mouthCostBuy / e).toFixed(2);

        $('#equipmentCost').text(equipmentCost);
        $('#managerDayCost').text(dayCostBuy);
        $('#managerMounthCost').text(mounthCostBuy);
        $('#liraxSum').text(mouthCostBuy);

        var specText1 = ('Количество модемов - ' + modemsCount + ' х 600грн = '+ modemsCount * 600 + 'грн<br>');
        var specText2 = ('Количество роутеров - ' + routersCount +' х 1800грн = '+  routersCount * 1800 +'грн<br><br>');
        var specText3 = ('Абонплата за модемы - ' + modemService + 'грн/мес<br>');
        var specText4 = ('Пополнение счета - ' + (modemsCount * 60 + 100) + 'грн/мес<br>');
        var specText5 = ('Лира офис - ' + liraOfficePlace * e + 'грн/мес');
        $('#xTremeTech').html(specText1 + specText2 + specText3 + specText4 + specText5);
    }
    liraxCalculator(10, 'true');

    
    var liraxCalculatorStandart = function(wp,nc) {
        var tarif = 50,
            numbersFree = Math.floor(wp / 2),
            differense = nc > numbersFree ? nc - numbersFree : 0,
            liraxSum = tarif * wp + differense * 20;
        $('#liraxWorkPlacesStandart .ui-slider-handle').html('<div class="workPlacesBlock">' + wp + '</div>');
        $('#liraxNumbersStandart .ui-slider-handle').html('<div class="workPlacesBlock">' + nc + '</div>');
        $('#managerDayCostStandart').text((liraxSum/30/wp).toFixed(2));
        $('#liraxSumStandart').text((liraxSum).toFixed(0));
    }
    liraxCalculatorStandart(5,2);

    updatePrices('currencyConvert', 32);

    // showSpecialPopup('newYearPopup');
});


// обновление цен в магазине
var updatePrices = function(value, exchange){
    var currency = parseFloat($('#data-currency').text());
    // isNaN(currency) ? currency = 28: currency = currency;
    $('[data-action=' + value + ']').each(function(){
        var discount = $(this).data('discount');
        discount ? discount = 1 - parseInt(discount) / 100 : discount = 1;
        $(this).text(numberToString(Math.round(parseFloat($(this).data('price')) * exchange * discount)));
    });
}

// разделяем числа на разряды
var numberToString = function(number){
    return String(number).replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

// Показ баннеров акционных
var showSpecialPopup = function(coockieData){
    if ($.cookie('lirax-special-popup') != coockieData && $.cookie(coockieData) != coockieData){
        getActionTemplate(coockieData);
    }
}

// Отключение показа баннеров
var disableSpecialPopup = function(coockieData){
    $.cookie(coockieData, 'off', {expires : 365});
    $.fancybox.close();
}

// Переход на акционную статью
// var getPresent = function(member, actionName){
//     $.cookie('lirax-special-popup', actionName, {expires : 1});
//     if (member == 'guest')
//         window.location.href = "http://lirax.com.ua/novogodnyaya-aktsiya-ot-kompanii-lirax/";
//     else
//         window.location.href = "http://lirax.com.ua/novogodnyaya-aktsiya-20-17-ot-kompanii-lirax/";

//     ga('send', 'event', 'present', member);
// }

// Подгрузка шаблонов для акционных баннеров
var getActionTemplate = function(actionName){
    tplDir = '/wp-content/themes/lirax-theme/tpl/';
    tplURLs = [
            tplDir + 'newYearPopup.tpl',
            tplDir + 'newYearPopup-2.tpl'
        ]
    options = {
            padding: 0,
            autoSize: false,
            autoHeight: true,
            maxWidth: 600,
            minHeight: 440,
            arrows: false,
            loop: false,
            nextEffect: 'fade',
            prevEffect: 'none',
            nextEffect: 'none',
            mouseWheel: false,
            scrolling: 'no',
            tpl: {
                wrap     : '<div class="fancybox-wrap fancybox-cristmas" tabIndex="-1"> \
                                <div class="fancybox-skin">\
                                    <div class="fancybox-outer">\
                                        <div class="fancybox-inner">\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>',
                closeBtn : '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"><i class="fa fa-snowflake-o" aria-hidden="true"></i></a>'
            },
            type: 'ajax',
            beforeClose: function(){
                $.cookie('lirax-special-popup', actionName, {expires : 1});
            }
        }
    $.fancybox(tplURLs, options);
}

var getFormDataString = function(form){
    var stringData = '', formData = form.serializeArray();
    
    $(formData).each(function(i, field) {
        var fieldName = updateFieldName(field.name), 
            fieldValue = field.value;
        if (fieldValue != 0)
            stringData = stringData + fieldName + ': ' + field.value + ' | ';
    });
    return stringData;
}

var updateFieldName = function(fieldName){
    var field = $('[name=' + fieldName + ']').data('name');
    if(field)
        return field;
    else
        return fieldName;
}
//Slider UA
$(document).ready(function() {
    var homeTitleDesc_UA = [
        'Допомагає керувати обробкою усіх звернень ваших клієнтів', 
        'Привертає нових кліентів, збільшує об’єми продаж',
        'Утримує клієнтів , зміцнює імідж компанії',
        'Знижує витрати на обробку вихідних дзвінків',
        'Підвищує якість обслуговування клієнтів'
    ];

    function miniSliderUA(array){
        var shuffleArray = array.shuffle();
        var arraySize = array.length, i = 0;
        $('#miniSliderUA').text(shuffleArray[i]);
        setInterval(function(){
            i++;
            $('#miniSliderUA').fadeOut(400, function(){
                $(this).text(shuffleArray[i]).fadeIn(400);
            });
            i >= arraySize ? i = 0 : i = i;
        }, 6000);
    }

    if($('*').is('#miniSliderUA')) {
        miniSliderUA(homeTitleDesc_UA);
    };
});