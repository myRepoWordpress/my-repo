$("#countOfUsersSlider").slider({
    range: "min",
    value: 1,
    min: 1,
    max: 30,
    slide: function(n, e) {
        $("#countOfUsers").val(e.value), calculator()
    }
}), $("#countOfChanelsSlider").slider({
    range: "min",
    value: 1,
    min: 1,
    max: 30,
    slide: function(n, e) {
        $("#countOfChanels").val(e.value), calculator()
    }
}), $("#countOfWorkPlacesSlider").slider({
    range: "min",
    value: 1,
    min: 2,
    max: 30,
    slide: function(n, e) {
        $("#countOfWorkPlaces").val(e.value), calculator()
    }
}), $("#countOfRingsSlider").slider({
    range: "min",
    value: 1,
    min: 1,
    max: 30,
    slide: function(n, e) {
        $("#countOfRings").val(e.value), calculator()
    }
});
var calculator = function() {
    var n, e, u, a, l, s, t = 2,
        r = 220,
        c = 15,
        o = 20,
        i = parseInt($("[name=countOfUsers]").val()),
        m = parseInt($("[name=countOfChanels]").val()),
        f = parseInt($("[name=countOfWorkPlaces]").val()),
        v = parseInt($("[name=countOfRings]").val());
    l = i + m, e = t > f ? t : f, sumPriceCountWorkPlace = r * e, a = 2 * e, sumCountOfLinesDefault = a >= v ? "" : v - a, sumPriceCountLines = c * sumCountOfLinesDefault, countOfNumbers = i + m, n = parseInt(e / 2), sumCountOfNumbers = n > countOfNumbers ? "" : countOfNumbers - n, u = o * sumCountOfNumbers, s = sumPriceCountWorkPlace + sumPriceCountLines + u, $("#summ").text(s), $("#hSumm").val(s), $("#totalNumbers").text(l)
};
calculator();